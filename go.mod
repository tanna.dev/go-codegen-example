module gitlab.com/tanna.dev/go-codegen-example

go 1.20

require (
	github.com/dave/jennifer v1.6.0
	gopkg.in/yaml.v3 v3.0.1
)
