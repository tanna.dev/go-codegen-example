# Generating code with github.com/dave/jennifer

An example of using [github.com/dave/jennifer](https://github.com/dave/jennifer) to perform code generation.

Goes alongside the blog post [_Automating boilerplate/scaffolding code with custom code generation in Go, with `jen`_](https://www.jvt.me/posts/2023/03/11/go-custom-generate-jen/
jk).

To execute, run:

```sh
go build
./jen -config conf.yaml
```

Then, inspect `errors.gen.go`.
