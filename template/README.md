# Using `text/template`

An example of using `text/template` to perform code generation.

Goes alongside the blog post [_Automating boilerplate/scaffolding code with custom code generation in Go_](https://www.jvt.me/posts/2022/06/26/go-custom-generate/).

To execute, run:

```sh
go build
./template -config conf.yaml
```

Then, inspect `errors.gen.go`.
