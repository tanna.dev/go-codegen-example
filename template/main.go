package main

import (
	"bytes"
	_ "embed"
	"flag"
	"go/format"
	"io/ioutil"
	"log"
	"os"
	"text/template"

	"gopkg.in/yaml.v3"
)

func must(err error) {
	if err != nil {
		log.Printf("There was an unexpected error: %s", err)
		os.Exit(1)
	}
}

//go:embed errors.tmpl
var errorsTemplate string

type config struct {
	Package string `yaml:"package"`
	Output  string `yaml:"output"`
	Errors  []struct {
		Name    string `yaml:"name"`
		Message string `yaml:"message"`
	} `yaml:"errors"`
}

func main() {
	configPathPtr := flag.String("config", "", "configuration file")
	flag.Parse()

	if configPathPtr == nil {
		log.Printf("Expected a configuration file, but received `nil`")
		os.Exit(1)
	}

	configPath := *configPathPtr
	b, err := ioutil.ReadFile(configPath)
	must(err)

	var config config

	err = yaml.Unmarshal(b, &config)
	must(err)

	t := template.Must(template.New("errors.go").Parse(errorsTemplate))

	var buf bytes.Buffer

	err = t.Execute(&buf, config)
	must(err)

	b, err = format.Source(buf.Bytes())
	must(err)

	ioutil.WriteFile(config.Output, b, 0644)
}
